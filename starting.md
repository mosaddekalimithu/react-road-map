# Create a New React App

### CLI Tool

We will use `vite` to create a new React app.

```shell
npm create vite@latest
```
Then we will be asked to enter the project name:

```shell
Project name: <app-name>
```

Select a framework:

```shell
Select a framework: React
```

Then select a variant:

```shell
Select a variant: TypeScript
```

We need to move to the app folder that we've just created and run the app:

```shell
cd <app-name>
npm install
npm run dev
```

Then we should see like this:
```shell
  VITE v4.3.3  ready in 410 ms

  ➜  Local:   http://localhost:5173/
  ➜  Network: use --host to expose
  ➜  press h to show help
```

Open `http://localhost:5173/` in browser and the React app should be available to use here.