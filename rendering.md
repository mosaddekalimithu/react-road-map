# Rendering

In React, rendering refers to the process of taking the component's state and props and turning them into a tree of HTML elements, which are then added to the DOM.

React uses a virtual DOM to keep track of changes and updates the actual DOM only when necessary. This allows for efficient updates and helps to improve the performance of the application.

The only HTML file that gets served is the `index.html` file present in the public folder. This HTML file contains a single `div` that is identified as the `root`. The rest of the application is only concerned within the `src` folder.

Within the `src` folder, the "root" of the application is in the `index.js` file. The line of importance is:

```jsx
ReactDOM.render(<App />, document.getElementById("root"));
```

The render method renders the application via the root component which is the App component and the DOM element with an `id` of `root`. This `root` is precisely the same `root` element that we encountered in our `index.html` file located in the public folder.

From here the App.js file will contain all of our custom components that we will use to build our application. The process is explained in greater detail in the link given below.

- [How React works under the hood](https://www.freecodecamp.org/news/react-under-the-hood/)

## Lists and Keys

In React, lists are often used to display a collection of data, such as a list of items, posts, or users. When rendering a list of items in a React component, it's important to use the `key` prop to help React keep track of each item.

Here's an example of how to render a list of items using the map function:

```jsx
function MyComponent() {
  const [list, setList] = useState([
    { id: 1, name: "user1" },
    { id: 2, name: "user2" },
    { id: 3, name: "user3" },
  ]);
  return (
    <ul>
      {list.map((item) => (
        <li key={item.id}>{item.name}</li>
      ))}
    </ul>
  );
}
```

In this example, the `list` variable is an array of objects, each with an `id` and a `name` property. The `map` function is used to iterate over the items and return a list of `<li>` elements. The `key` prop is used to set the `key` attribute on each `<li>` element, with the value of the item's `id` property.

The key prop helps React to keep track of the elements in the list, so it can efficiently update the DOM when the list changes. Without a key, React has to re-render the entire list every time an item is added or removed. The `key` should be unique among its siblings and should not change on re-renders. A common practice is to use the `ID` or a unique property of the item provided by the server.

- [Examples](https://www.robinwieruch.de/react-list-component/)

> Using the `index` of an item as the `key` when rendering a list is generally not recommended. It is not guaranteed to be unique and may lead to poor performance and unexpected behavior. [Best practices for `key` prop](https://www.robinwieruch.de/react-list-key/).

## Render Props

Render props is a pattern in React that allows a component to share its logic with another component, without having to use inheritance or higher-order components.

A component that uses the render props pattern will expose a single prop that is a function. This function will be called by the component, and it should be used by the consuming component to render its content.

Here's an example of a simple component that uses the render props pattern:

```jsx
function MyComponent({ render }) {
  const data = { name: "John", age: 30 };
  return render(data);
}
function App() {
  return (
    <MyComponent
      render={(data) => (
        <p>
          {data.name} is {data.age} years old
        </p>
      )}
    />
  );
}
```

In this example, the `MyComponent` component has a single prop called `render`, which is a function. The `MyComponent` component calls this function and passes it some data, which is used by the consuming component to render its content. In this case, the data is an object with a `name` and `age` properties.

The consuming component `App` uses the `MyComponent` component and passes it a render function, which takes the data object and renders a `p` element with the name and age.

Render Props allows to share the component's internal state or logic with other components, making the components more reusable. It also allows to handle the component's logic and state at the parent level, which makes the component more flexible and easy to test.

- [Examples](https://www.robinwieruch.de/react-render-props/)

## Refs

In React, `refs` are used to access the underlying DOM elements of a component or a DOM node. In a functional component, refs can be used to access a DOM element or a child component.

In functional components, you can use the `useRef` hook to create a ref. Here is an example:

```jsx
import { useRef } from "react";

function MyComponent() {
  const inputRef = useRef(null);

  const handleClick = () => {
    inputRef.current.focus();
  };

  return (
    <div>
      <input ref={inputRef} type="text" />
      <button onClick={handleClick}>Focus Input</button>
    </div>
  );
}
```

In this example, the `useRef` hook is used to create a ref named `inputRef` with an initial value of `null`. The ref is then passed as the `ref` prop to the `input` element. Inside the `handleClick` function, we use the `current` property of the ref to access the underlying `input` element and call the `focus` method.

It's also worth noting that refs created using `useRef` will persist across re-renders, so we can use the ref to access the element even after the component has been updated.

- [Guide to `useRef()`](https://dmitripavlutin.com/react-useref-guide/)

- [Video Guide](https://www.youtube.com/watch?v=t2ypzz6gJm0&ab_channel=WebDevSimplified)

## Events

Handling events with React elements is very similar to handling events on DOM elements. There are some syntax differences:

- React events are named using camelCase, rather than lowercase.
- With JSX we pass a function as the event handler, rather than a string.

To handle an event in a React component, we can use an event handler function. This function is called whenever the specified event occurs. Here is an example of a simple event handler function that handles a button click:

```jsx
function MyComponent() {
  const handleClick = () => {
    console.log("1st Button was clicked!");
  };

  return (
    <>
      <button onClick={handleClick}>Click me</button>
      <button onClick={() => console.log("2nd Button was clicked!")}>
        Click me
      </button>
    </>
  );
}
```

In this example, the `handleClick` function is called whenever the first button is clicked. The `onClick` prop is used to attach the event handler function to the button. We can also pass an `anonymous function` as the event handler, like the second button.

- [Examples](https://www.robinwieruch.de/react-event-handler/)
- [React Events](https://reactjs.org/docs/events.html)

## Higher-Order Components

In React, a higher-order component (HOC) is a pattern that allows you to reuse component logic. A HOC is a function that takes a component as an argument and returns a new component with additional functionality or props.

Here is an example of a simple HOC that adds a `isAdmin` prop to a component:

```jsx
function withAdmin(WrappedComponent) {
  return function (props) {
    return <WrappedComponent {...props} isAdmin={true} />;
  };
}

const MyComponentWithAdmin = withAdmin(MyComponent);
```

In this example, the `withAdmin` function is a HOC that takes a component (`WrappedComponent`) as an argument and returns a new component that adds a `isAdmin` prop to the wrapped component. The new component is named `MyComponentWithAdmin`.

- [Official Doc](https://reactjs.org/docs/higher-order-components.html)
- [Examples](https://www.robinwieruch.de/react-higher-order-components/)
- [Video Guide](https://www.youtube.com/watch?v=B6aNv8nkUSw&ab_channel=Codevolution)
