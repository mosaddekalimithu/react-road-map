### React Hooks

Hooks are functions that allow you to use state and other React features in functional components. They were introduced in React 16.8 as a way to use state and other features in functional components, instead of having to use class components.

There are several built-in hooks that come with React, such as useState, useEffect, and useContext.

* [Official Doc](https://react.dev/reference/react)

* [Video Guide](https://www.youtube.com/watch?v=cF2lQ_gZeA8&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&ab_channel=Codevolution)

## useState Hook

The `useState` hook is a built-in hook in React that allows us to add state to a functional component. It takes an initial value as an argument, and returns an array containing the current state and a function to update it.

Here is an example of `useState` hook to add a `count` state to a component:

```jsx
import { useState } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  return (
    <>
      <p>The count is {count}</p>
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <button onClick={() => setCount(count - 1)}>Decrement</button>
    </>
  );
}
```

In this example, `useState` is called with an initial value of 0. It returns an array containing the current state (`count`) and a function to update the state (`setCount`). The component renders a paragraph displaying the current count, and two buttons that increment and decrement the count when clicked.

When the component is first rendered, the `count` state will be set to the initial value of 0. When the "Increment" button is clicked, the `setCount` function is called with a new value of count + 1. This updates the state and causes the component to re-render with the new value of `count`. 

* [Official Doc](https://react.dev/reference/react/useState)

* [Example](https://www.robinwieruch.de/react-usestate-hook/)

* [Video Guide](https://www.youtube.com/watch?v=lAW1Jmmr9hc&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=2&ab_channel=Codevolution)

## useEffect Hook

The `useEffect` hook is a built-in hook in React that allows us to run side effects, such as fetching data or updating the DOM, in response to changes in our component. It takes a function that contains the side effect code, and an array of dependencies.

Here's an example of using the `useEffect` hook with a dependency:
```jsx
import { useState, useEffect } from "react";

function MyComponent({ userId }) {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    fetch(`https://example.com/api/users/${userId}`)
      .then((response) => response.json())
      .then((data) => setUserData(data));
  }, [userId]);

  return (
    <div>
      {userData ? (
        <div>
          <h1>{userData.name}</h1>
          <p>{userData.bio}</p>
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}
```
In the above example, the `useEffect` hook is used to fetch user data from an API when the userId prop changes. The `useEffect` hook takes two arguments: a function that runs the side effect, and an array of dependencies that specify when the effect should be re-run. In this case, the `useEffect` hook will only re-run when the userId value changes.

If we want to run a side effect only once, when the component mounts, we can pass an empty array `[]` as the second argument of `useEffect`.

Here is an example of the `useEffect` hook to fetch data from an API and update the component state:

```jsx
import { useState, useEffect } from 'react';

function DataFetcher() {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch('https://api.example.com/data')
      .then(response => response.json())
      .then(data => setData(data));
  }, []);

  return <div>{data ? data.message : 'Loading...'}</div>;
}
```

In this example, `useEffect` is called with a function that makes a fetch request to an `API` and updates the data state with the response. 

We pass an array of dependencies as a second argument to the `useEffect` function. When the dependencies change, the effect will re-run. If we pass an empty array, the effect will only run once, when the component is mounted.

Another case, let's say we have a component that fetches some data from an API when it mounts. We want to clean up the fetch request when the component unmounts, to prevent memory leaks and other issues.

```jsx
import { useEffect, useState } from "react";

function MyComponent() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("https://example.com/api/data");
      const json = await response.json();
      setData(json);
    };

    fetchData();

    return () => {
      controller.abort();
    };
  }, []);

  return (
    <div>
    {data.length ? (
      <ul>
        {data.map((item) => (
          <li key={item.id}>{item.name}</li>
        ))}
      </ul>
    ) : (
      <div>No data found.</div>
    )}
  </div>
  );
}
```
In the above example, we define a function fetchData that fetches some data from an API and sets it to the data state using the setData function. We call this function in the `useEffect` hook when the component mounts, and also return a cleanup function that aborts the fetch request using an `AbortController`.

The `AbortController` API is a modern way of cancelling a fetch request. We can create a new `AbortController` object, which gives us an `AbortSignal` object that we can pass to the `fetch` function. Then, in the cleanup function, we can call the `abort` method of the `AbortController` to cancel the fetch request.

It's important to note that the `useEffect` function will run on every render by default, if we don't specify any dependency, this can lead to infinite loops or unwanted behavior, so it's important to specify the dependencies correctly.

* [Official Doc](https://react.dev/reference/react/useEffect)

* [Tips for useEffect hook](https://reactjs.org/docs/hooks-effect.html#tips-for-using-effects)

* [Video Guide](https://www.youtube.com/watch?v=06Y6aJzTmXY&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=6&ab_channel=Codevolution)

## useContext

`useContext` is a hook that allows us to access context in a functional component. Context allows us to share data and state between components without having to pass props through multiple levels of the component tree.

Here is an example of how you might use useContext to access a theme context in a component:

```jsx
import { createContext, useContext } from 'react';

const ThemeContext = createContext('light');

function ThemeToggler() {
  const theme = useContext(ThemeContext);

  return <button>Toggle theme ({theme})</button>;
}

function App() {
  const [theme, setTheme] = useState('light');

  return (
    <ThemeContext.Provider value={theme}>
      <ThemeToggler />
      <button onClick={() => setTheme(theme === 'light' ? 'dark' : 'light')}>
        Toggle theme
      </button>
    </ThemeContext.Provider>
  );
}

```
In this example, `createContext` is used to create a new context with a default value of 'light'. The `ThemeToggler` component uses the `useContext` hook to access the theme context. The component will always have access to the current theme value, regardless of where it is rendered in the component tree.

The `App` component is the parent component that wraps the `ThemeToggler` component, and it uses the `ThemeContext.Provider` component to provide the current theme value to all its children. The provider component takes a `value` prop which is the current theme value.

The `useContext` hook is passed the context object created by `createContext` and it returns the current value of the context.

* [Official Doc](https://react.dev/reference/react/useContext)

* [Video Guide](https://www.youtube.com/watch?v=CI7EYWmRDJE&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=15&ab_channel=Codevolution)

## useReducer

`useReducer` is a built-in hook in React that allows us to manage state in a functional component. It is similar to `useState`, but it is more powerful and is recommended when the component state is complex and changes in multiple ways.

`useReducer` takes two arguments: a reducer function and an initial state. The reducer function is a pure function that takes in the current state and an action, and returns the new state. The initial state is the initial value of the state.

Here is an example of how you might use `useReducer` to manage a counter state:

```jsx
function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    default:
      throw new Error();
  }
}

function Counter() {
  const initialState = {count: 0};
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({type: 'decrement'})}>-</button>
      <button onClick={() => dispatch({type: 'increment'})}>+</button>
    </>
  );
}
```

In this example, `reducer` is a function that takes in the current state and an action. It uses a switch statement to determine the new state based on the action type. If the action type is `increment`, the function returns a new state object with the count property incremented by 1. If the action type is `decrement`, the function returns a new state object with the count property decremented by 1. If the action type is anything else, the function throws an error.

The `Counter` component uses the `useReducer` hook to manage its state. It calls `useReducer` and passes in the `reducer` function and the `initialState`. `useReducer` returns an array containing the current state (`state`) and a function to dispatch actions (`dispatch`).

* [Official Doc](https://react.dev/reference/react/useReducer)

* [Video Guide](https://www.youtube.com/watch?v=cVYp4u1m6iA&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=18&ab_channel=Codevolution)

## useMemo

`useMemo` is a hook that allows us to optimize the performance of a component by only recalculating a value when one of its `dependencies` has changed. This optimization helps to avoid expensive calculations on every render.

`useMemo` takes two arguments: a function that returns the value you want to memoize, and an array of `dependencies`. It returns the memoized value.

* [Example](https://www.geeksforgeeks.org/react-js-usememo-hook/)

* [Official Doc](https://react.dev/reference/react/useMemo)

* [Video Guide](https://www.youtube.com/watch?v=qySZIzZvZOY&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=27&ab_channel=Codevolution)