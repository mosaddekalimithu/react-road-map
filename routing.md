# Routing

Routing is the process of handling client-side navigation within a web application. It enables the application to navigate between different pages or components without requiring a full page reload.

There are several libraries available for implementing routing in a React application. The most popular one is React Router.

## React Router

`React Router` is a library that provides a set of components and hooks for handling client-side routing in a React application. It allows us to define different routes for different pages or components in our application and handles the navigation between them.

```jsx
import { BrowserRouter, Route, Link } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/contact">Contact</Link>
      </nav>
      <Route exact path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/contact" component={Contact} />
    </BrowserRouter>
  );
}
```

In this example, the `BrowserRouter` component is used to wrap the entire application. It provides the `location` object and other information about the current route to the rest of the components.

The `Link` component is used to create links that navigate between routes. It takes a `to` prop that specifies the path of the route to navigate to.

The `Route` component is used to define the different routes in the application. It takes a `path` prop that specifies the path of the route and a `component` prop that specifies the component that should be rendered when the route is matched.

In this example, when the user navigates to the root path `/`, the `Home` component is rendered. When the user clicks the `About` link, the `About` component is rendered and so on.


* [Official Doc](https://reactrouter.com/en/main/start/overview)

* [Video Guide](https://www.youtube.com/watch?v=Ul3y1LXxzdU&ab_channel=WebDevSimplified)

* [Video Guide 2](https://www.youtube.com/watch?v=0cSVuySEB0A&ab_channel=HiteshChoudhary)

>Both the Video Guides explains the basic of React Router. Following one should be enough to get the basic idea of routing.