# Components

Components are the building blocks of React applications. They let us split the UI into independent, reusable pieces, and think about each piece in isolation.
The diagram below illustrates this concept visually.
<br>
<img src="https://spectrum-bd.biz/data//projects/02-sscl/react-redux-roadmap/component-layout.png" alt="component" width="400" height="200">

## Components Types

* [Class Components](https://www.w3schools.com/react/react_class.asp)
* [Functional Components](https://www.geeksforgeeks.org/reactjs-functional-components/)

> We will use Functional Components for our projects. Knowledge on Class Components may only be required when we have to work with old projects that are based on Class Components. So we will only discuss Functional Components.

## Class Components

A class-based component is defined as a JavaScript class that extends the React.Component class. The class must have a render method, which returns the JSX code that represents the component's UI.

```jsx
class MyComponent extends React.Component {
  render() {
    return <div>Hello, world!</div>;
  }
}
```
## Functional Components

Functional components are some of the more common components that will come across while working in React. These are simply JavaScript functions. We can create a functional component to React by writing a JavaScript function. These functions may or may not receive data as parameters. In the functional Components, the return value is the JSX code to render to the DOM tree. Functional components can also have state which is managed using React hooks.

```jsx
function MyComponent(props) {
  return <div>Hello, world!</div>;
}
```

* [Example](https://www.geeksforgeeks.org/reactjs-functional-components/)

## JSX

JSX (JavaScript XML) is a syntax extension for JavaScript that allows us to write HTML-like elements and components in our JavaScript code. It is used in React to define the structure and content of a component.

JSX code looks similar to HTML, but with a few key differences. For example, instead of using class to define a CSS class, we use className, and instead of using HTML elements, we use React components.  

Here is an example of JSX code:
```jsx
const element = <h1 className="greeting">Hello, world!</h1>;
```  

In this example, element is a JSX element that represents a h1 heading with the text "Hello, world!" and a CSS class of "greeting".

* [Official Doc](https://reactjs.org/docs/introducing-jsx.html)

* [W3school](https://reactjs.org/docs/introducing-jsx.html)

* [Example](https://www.freecodecamp.org/news/jsx-in-react-introduction/)

## Component State

In React, the component state is an object that holds the data specific to a component. It is used to store and manage data that can change over time and can affect the component's behavior or rendering.

In functional component state is managed using the `useState` hook.

```jsx
function MyComponent() {
  const [count, setCount] = useState(0);
  return (
    <div>
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <div>Count: {count}</div>
    </div>
  );
}
```

In this example, the `useState` hook is used to initialize the state and to create a `count` variable and a `setCount` function that can be used to update the state.

It's important to note that we should avoid updating the state based on the current state, as this can lead to unexpected results. Instead, it's better to use functional updates, by passing a function to the `setCount` method, this way it will receive the current state, and we can use it to calculate the next state.

```jsx
<button onClick={() => setCount(prevState => prevState+1)}>Increment</button>
```

Here `prevState` will have the value from the previous state.

Another important note is that use `onClick={methodName}` when you want to pass a reference to a method that should be called when the element is clicked. For example:
```jsx
function handleClick() {
  console.log("Button clicked!");
}

<button onClick={handleClick}>Click me</button>
```
In the above example, the `handleClick` method is passed as a reference to the `onClick` prop of the button element. When the button is clicked, the `handleClick` method is called.

Use `onClick={() => methodName(value)}` when you need to pass an argument to the method that should be called when the element is clicked. For example:
```jsx
function handleItemClick(item) {
  console.log(`Item clicked: ${item}`);
}

const items = ["apple", "banana", "orange"];

<ul>
  {items.map((item) => (
    <li key={item} onClick={() => handleItemClick(item)}>
      {item}
    </li>
  ))}
</ul>
```
In the above example, an array of items is mapped to a list of `<li>` elements. Each `<li>` element has an `onClick` prop that calls the `handleItemClick` method with the corresponding item value as its argument.

> You might find it helpful to explore the [State Handling](https://reactjs.org/docs/faq-state.html) in Class Components if you are working on a project that uses class components instead of functional components.

## Props

React has a special object called a prop (stands for property) which we use to transport data from one component to another. Props only transport data in a one-way flow (only from parent to child components). It is not possible with props to pass data from child to parent, or to components at the same level.  

In a functional component, props are passed as an argument to the function, just like any other parameter.
Here is an example of a functional component that receives props:

```jsx
function ChildComponent(props) {
  return <div>My name is {props.name} and I am {props.age} years old.</div>;
}
function ParentComponent(props) {
  return <ChildComponent name={''} age={10}/>;
}
```

In this example, the ChildComponent function receives an object called props as an argument, which contains the props passed to the component. The component then uses the name and age properties of the props object to render its UI.

We can also use destructuring assignment to directly extract the props we need from the props object

```jsx
function ChildComponent({name, age}) {
  return <div>My name is {name} and I am {age} years old.</div>;
}
```

* [Official Doc](https://react.dev/learn/passing-props-to-a-component)

## State and Props


### Derive Initial State From Props

In a React functional component, we can use the useState hook to create a state variable and set its initial value based on a prop passed to the component. Here's an example:

```jsx
import { useState } from 'react';

function MyComponent(props) {
  const [name, setName] = useState(props.userName);
  return (
    <p>{name}</p>
  );
}

```

In this example, the useState hook is used to create a state variable named `name` and set its initial value to `props.userName`. The `userName` prop is passed to the MyComponent function as an argument, and is used to set the initial value of the `name` variable.

* [Example](https://www.robinwieruch.de/react-derive-state-props/#how-to-derive-initial-state-from-props-in-react)

* [Update State From Props](https://www.robinwieruch.de/react-derive-state-props/#how-to-update-state-from-props-in-react)

* [State vs Props](https://www.geeksforgeeks.org/what-are-the-differences-between-props-and-state/)

## Conditional Rendering

In React, we can use conditional rendering to render different parts of the component's UI based on conditions. There are several ways to implement conditional rendering in React. We can use `if-else`, `&&` operator, [ternary operator](https://www.w3schools.com/react/react_es6_ternary.asp).  
Here is an example of a component that conditionally renders a message based on a boolean prop:

```jsx
function MyComponent({isVisible}) {
    if (isVisible) {
      return <div>The component is visible</div>;
    } else {
      return <div>The component is hidden</div>;
    }
}
```

In this example, the component checks the value of the isVisible prop. If it is true, it renders a message saying "The component is visible", else renders "The component is hidden".  
we can also use the ternary operator for the same purpose.

```jsx
function MyComponent({isVisible}) {
      return (
          isVisible ? <div>The component is visible</div> : <div>The component is hidden</div>
      );
}
```

* [Example](https://www.robinwieruch.de/conditional-rendering-react/)

## Composition

In React, composition refers to the process of building components by combining other smaller, reusable components. This allows us to create complex UI elements by breaking them down into smaller, more manageable parts.

Here is an example of a simple component that is composed of other smaller components:
```jsx
function ParentComponent() {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
}
```

In this example, the ParentComponent is composed of three smaller components: Header, Body, and Footer. Each of these components can be defined and reused separately, which makes it easier to manage and maintain the code.

We can also use React Fragments to rewrite the ParentComponent function without adding an additional div element to the rendered output. Here's an example:
```jsx
function ParentComponent() {
  return (
    <>
      <Header />
      <Body />
      <Footer />
    </>
  );
}
```
In the above example, the `<>` and `</>` tags are shorthand for an empty React Fragment. A React Fragment lets you group a list of children without adding extra nodes to the DOM.

* [Official Doc](https://react.dev/reference/react/Fragment)

* [Example](https://felixgerschau.com/react-component-composition/#:~:text=Conclusion-,What%20is%20component%20composition%20in%20React%3F,new%20components%20with%20other%20components)

[https://spectrum-bd.biz/data//projects/02-sscl/react-redux-roadmap/component-layout.png]: https://spectrum-bd.biz/data//projects/02-sscl/react-redux-roadmap/component-layout.png