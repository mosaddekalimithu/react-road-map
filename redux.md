# Redux

Redux is a JavaScript library that is commonly used with React to manage the state of an application. It is based on the
concept of a `store` which holds the state of the entire application, and `actions` which are used to update that state.
Redux follows the principles of the Flux architecture and is designed to work well with other libraries and frameworks,
like React.

Redux allows us to:

* Centralize the state of your application in a single location, making it easier to manage and debug.

* Use a predictable and consistent pattern for updating the state of your application, making it easier to reason about
  and test.

* Easily implement features like undo/redo and time-travel debugging.

[Redux Introduction (Video)](https://www.youtube.com/watch?v=0awA5Uw6SJE&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=1&ab_channel=Codevolution)

## Three Core Components

* The Store: A JavaScript object that holds the current state of the application.
* Actions: JavaScript objects that describe the intended change to the state of the application.
* Reducers: JavaScript functions that take the current state of the application, an action, and return the new state.

[Core Concepts (Video)](https://www.youtube.com/watch?v=7cREd9mesMg&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=3&ab_channel=Codevolution)

## Three Principles

Redux is based on three main principles:

* Single source of truth: The state of the entire application is stored in a single object called the store. This makes
  it easy to manage and debug the state of the application.

* State is read-only: The state can only be updated by dispatching actions, which are plain JavaScript objects that
  describe the intended change. This ensures that the state can only be updated in a predictable and consistent way.

* Changes are made with pure functions: Reducers are pure functions that take the current state and an action and return
  the new state. They are responsible for updating the state based on the actions that are dispatched to the store. This
  ensures that the state can only be updated in a predictable and consistent way.

These principles make it easy to reason about the state of the application, test the application, and implement features
like undo/redo and time-travel debugging.

* [Three Principles (Video)](https://www.youtube.com/watch?v=6oAU5Kn9SBY&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=4&ab_channel=Codevolution)

## Store

The Redux store is the central location where the state of your application is stored. The store holds the current state
of the application, and it's the single source of truth for the state. The store is an object that is created by
the `createStore() `function from the `redux` library.

The store has the following responsibilities:

* Hold the current state of the application.
* Allow us to dispatch actions to update the state.
* Allow us to subscribe to the store to be notified when the state changes.
* Allow us to access the current state at any time.

> [Redux Store](https://www.youtube.com/watch?v=WDJ2eidE-b0&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=7&ab_channel=Codevolution)

## Actions

In Redux, actions are JavaScript objects that describe an intended change to the state of the application. They are
dispatched to the store, and the store's reducers use them to update the state. Actions typically have a `type` property
that describes the type of action and a `payload` property that carries the data that is required to update the state.

Actions are created using action creator functions, which are simple JavaScript functions that return an action object.
These functions make it easy to create actions, encapsulate the logic of creating the action, and make the code more
readable.

Here is an example of a simple action:

```jsx
const increment = (value) => {
    return {
        type: 'INCREMENT',
        payload: value
    }
}
```

> [Redux Actions](https://www.youtube.com/watch?v=ir0F2PeJugo&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=5&ab_channel=Codevolution)

## Reducers

A reducer is a function that takes the current state of the application, an action, and returns the new state. Reducers
are responsible for updating the state of the application based on the actions that are dispatched to the store. They
are the only way to update the state and should be pure functions, meaning that they should not have any side effects
and should always return the same output given the same input.

Here is an example of a simple reducer:

```jsx
const reducer = (state, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.payload
            };
        case 'DECREMENT':
            return {
                count: state.count - action.payload
            };
        default:
            return state;
    }
}
```

> [Redux Reducer](https://www.youtube.com/watch?v=fy2FHo-iXDE&list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3&index=7&ab_channel=Codevolution)

## Redux Application One-Way-Data-Flow

The one-way data flow in Redux means that the application's state can only be changed by dispatching actions that are then handled by reducers, ensuring a predictable and maintainable data flow.

* State describes the condition of the app at a specific point in time

* The UI is rendered based on that state

* When something happens (such as a user clicking a button), the state is updated based on what occurred

* The UI re-renders based on the new state

Visualize the below diagram:

<img src="https://d33wubrfki0l68.cloudfront.net/01cc198232551a7e180f4e9e327b5ab22d9d14e7/b33f4/assets/images/reduxdataflowdiagram-49fa8c3968371d9ef6f2a1486bd40a26.gif" alt="redux-one-way-data-flow" width="400" height="300" />

> [Official Doc](https://redux.js.org/tutorials/fundamentals/part-2-concepts-data-flow)

## Redux Toolkit (RTK)

Redux Toolkit is a official package from the Redux team that makes it easier to write Redux logic by providing a set of
tools and utilities. It provides a simplified way to write actions, reducers, and selectors while leveraging the power
of Redux.

### Redux Toolkit includes features

#### createSlice

The `createSlice` is a function that generates a reducer and action creators for a specific piece of state.
To use `createSlice`, you'll need to import it from the `@reduxjs/toolkit` package:

```jsx
import { createSlice } from "@reduxjs/toolkit";
```

Next, we can define our initial state as an object:

```jsx
const initialState = {
  count: 0
};
```

Then, call `createSlice` and pass it an object that defines the name of our slice, the initial state, and any reducers we want to define:

```jsx
const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    increment: (state, action) => {
      state.count++;
    },
    decrement: (state, action) => {
      state.count--;
    }
  }
});
```

The `createSlice` function will automatically generate action creators and action types for each of the reducers we define in the `reducers` object.

The resulting `counterSlice` object will have two properties: `reducer` and `actions`. The `reducer` property is a function that we can pass to the `createStore` function from Redux, and the `actions` property is an object that contains the generated action creators.

Finally, we can export the `reducer` and `actions` properties from our module:

```jsx
export const reducer = counterSlice.reducer;
export const counterActions = counterSlice.actions;
```

#### configureStore

The `configureStore` is a function that sets up the Redux store with sensible defaults, including support for middleware and enhancers.

To use `configureStore`, we'll need to import it from the `@reduxjs/toolkit` package:

```jsx
import { configureStore } from "@reduxjs/toolkit";
```

Next, we can pass it an object that defines the configuration options for our store:

```jsx
const store = configureStore({
  reducer: {
    counter: counterReducer
  }
});
```

In this example, we're passing an object with a `reducer` property that maps the `counter` state to the `counterReducer`.

The `configureStore` function automatically includes the `redux-thunk` middleware and sets up the `Redux DevTools` extension, if it's available.

> [Official Doc](https://redux-toolkit.js.org/api/configureStore)

#### createAsyncThunk

A utility for creating asynchronous thunks that handle loading, error, and success states.

To use `createAsyncThunk`, we'll need to import it from the `@reduxjs/toolkit` package:

```jsx
import { createAsyncThunk } from "@reduxjs/toolkit";
```

Next, we can pass it two arguments: a string that defines the name of the Thunk action and a function that returns a Promise representing the asynchronous action:

```jsx
const fetchUserById = createAsyncThunk(
  'users/fetchByIdStatus',
  async (userId, thunkAPI) => {
    const response = await fetch(`/api/users/${userId}`)
    return response.json()
  }
)
```
In this example, we're creating a Thunk action called `fetchUserById` that fetches user data from an API. The second argument to `createAsyncThunk` is an `async` function that takes two arguments: the `userId` parameter passed to the Thunk action and a `thunkAPI` object that provides utilities like `dispatch` and `getState`.

`createAsyncThunk` automatically generates three action types based on the name of the Thunk action: `{typePrefix}/pending`, `{typePrefix}/fulfilled`, and `{typePrefix}/rejected`. For example, the `fetchUserById` Thunk action will generate the following action types:

* `users/fetchByIdStatus/pending`
* `users/fetchByIdStatus/fulfilled`
* `users/fetchByIdStatus/rejected`

The `createAsyncThunk` function returns a Redux Thunk action that can be dispatched like any other Redux action:

```jsx
dispatch(fetchUserById(userId))
```

When the Thunk action is dispatched, it will automatically dispatch the `{typePrefix}/pending` action, and then call the `payloadCreator` function. If the Promise returned by `payloadCreator` resolves, the Thunk action will dispatch the `{typePrefix}/fulfilled` action with the resolved value as the payload. If the Promise is rejected, the Thunk action will dispatch the `{typePrefix}/rejected` action with the error as the payload.

Note that in our project, we will be using RTK Query instead of createAsyncThunk. With RTK Query, we can easily create API requests using a declarative syntax, handle loading and error states, cache API responses, and more. It also provides a built-in mechanism for pagination and polling. Using RTK Query can greatly simplify our code and improve our API request management.

> [Official Doc](https://redux-toolkit.js.org/api/createAsyncThunk)

#### immer

A library that allows you to work with state immutably, making it easier to write reducers that update the state.

By using Redux Toolkit, developers can write less boilerplate code and focus more on the logic of their application.


## RTK Query

RTK Query is a powerful data fetching and caching tool. It is designed to simplify common cases for loading data in a web application, eliminating the need to hand-write data fetching & caching logic yourself.

RTK Query is an optional addon included in the Redux Toolkit package, and its functionality is built on top of the other APIs in Redux Toolkit.

### What problem does it solve

* Tracking loading, error and success state
* Avoid duplicate requests for some data
* Optimistic updates to makes the UI feels faster
* Managing cache lifetimes as the user interacts with the UI

### RTK Query Includes Features
#### Create API Slice
Here we've an example of `createApi` using rtk query. Let's explore the example line by line.

```jsx
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiSlice = createApi({
    reducerPath: "api",
    baseQuery: fetchBaseQuery({
        baseUrl: "http://localhost:9000",
    }),
    endpoints: (builder) => ({
        getVideos: builder.query({
            query: () => "/videos",
        }),
        getVideo: builder.query({
            query: (id) => `/videos/${id}`,
        }),
        addVideo: builder.mutation({
            query: (data) => ({
                url: "/videos",
                method: "POST",
                body: data,
            })
        }),
        editVideo: builder.mutation({
            query: ({ id, data }) => ({
                url: `/videos/${id}`,
                method: "PATCH",
                body: data,
            })
        }),
        deleteVideo: builder.mutation({
            query: (id) => ({
                url: `/videos/${id}`,
                method: "DELETE",
            })
        }),
    }),
});

export const {
    useGetVideosQuery,
    useGetVideoQuery,
    useAddVideoMutation,
    useEditVideoMutation,
    useDeleteVideoMutation,
} = apiSlice;
```

The provided code demonstrates the usage of RTK Query, a package provided by Redux Toolkit for performing API requests in a declarative way with minimal boilerplate.

The `createApi` function from RTK Query is used to create an API object. It takes an object with the following properties as arguments:

* `reducerPath`: A string that represents the path where the RTK Query reducer will be mounted in the Redux store.

* `baseQuery`: An instance of a base query that is used for making requests to the API. The `fetchBaseQuery` function is a helper provided by RTK Query that returns a base query using the fetch API.

* `endpoints`: An object containing a set of endpoints that define the queries and mutations to be used with the API.

In this specific code, the `apiSlice` object is created with the above-mentioned properties. It contains endpoints for `getting`, `adding`, `editing`, and `deleting` videos.

For the `getVideos` endpoint, a query is defined that returns a list of videos by making a `GET` request to the `/videos` endpoint. The `useGetVideosQuery` hook is generated automatically from this endpoint.

Similarly, for the `getVideo` endpoint, a query is defined that takes a `id` argument and returns the video with the corresponding `ID` by making a `GET` request to the `/videos/:id` endpoint. The `useGetVideoQuery` hook is generated automatically from this endpoint.

For the `addVideo` endpoint, a mutation is defined that takes a `data` argument and sends a `POST` request to the `/videos` endpoint with the data as the request body. The `useAddVideoMutation` hook is generated automatically from this endpoint.

For the `editVideo` endpoint, a mutation is defined that takes an id and data argument and sends a `PATCH` request to the `/videos/:id` endpoint with the data as the request body. The `useEditVideoMutation` hook is generated automatically from this endpoint.

For the `deleteVideo` endpoint, a mutation is defined that takes an `id` argument and sends a `DELETE` request to the `/videos/:id` endpoint. The `useDeleteVideoMutation` hook is generated automatically from this endpoint.

These hooks can then be used in a React component to query or mutate the `API`. The hooks automatically handle the loading and error states, and cache `API` responses to improve performance. The `reducerPath` can be used to select the `slice` of the Redux store where the result of the `API` requests will be stored.


> [Project Example](https://codesandbox.io/p/github/golammostafa13/think-in-a-redux-way/draft/festive-tereshkova?file=%2Fsrc%2Fcomponents%2Fvideos%2FVideos.js&selection=%5B%7B%22endColumn%22%3A15%2C%22endLineNumber%22%3A15%2C%22startColumn%22%3A15%2C%22startLineNumber%22%3A15%7D%5D&workspaceId=0a255cc0-7833-4852-93be-0253bfe417c9)

## Resources

* [Redux and RTK tutorial (Codevolution)](https://www.youtube.com/playlist?list=PLC3y8-rFHvwiaOAuTtVXittwybYIorRB3)

* [RTK Query tutorial](https://www.youtube.com/watch?v=YWbZILe35P4&t=229s&ab_channel=DipeshMalvia)

