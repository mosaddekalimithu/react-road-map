# Plan

## Prerequisites

* [JavaScript](https://www.youtube.com/watch?v=hdI2bqOjy3c&list=PLillGF-RfqbbnEGy3ROiLWk7JMCuSyQtX&ab_channel=TraversyMedia)

* [Es6](https://www.youtube.com/watch?v=2LeqilIw-28&list=PLillGF-RfqbZ7s3t6ZInY3NjEOOX7hsBv&ab_channel=TraversyMedia)

* [Node.js](https://www.w3schools.com/nodejs/nodejs_intro.asp)

* [TypeScript](https://www.youtube.com/watch?v=BCg4U1FzODs&t=666s&ab_channel=TraversyMedia)

* [git](https://www.youtube.com/watch?v=SWYqp7iY_Tc&ab_channel=TraversyMedia)

* [npm](https://www.w3schools.com/whatis/whatis_npm.asp)

## Roadmap

### [Getting Started](./starting.md)

### [Components](./components.md)

### [Rendering](./rendering.md)

### [Hooks](./hooks.md)

### [Routing](./routing.md)

### [State Management (Redux)](./redux.md)

### [Form Handling](./forms.md)


