# Forms

A `form` is an HTML element that allows users to input data and submit it to a server for processing. In react we also use `form` to interact with the backend.


## Managing form state in react

In React, forms are created by using the `form` element and its related form controls such as `input`, `select`, and `textarea`. The form controls are managed as part of the component state, and their values are updated in response to user input.

Here is an example of a simple form component that collects user information and logs it:

```jsx
import React, { useState } from "react";

function UserForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(name, email);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input
          type="text"
          name="name"
          value={name}
          onChange={(event) => setName(event.target.value)}
        />
      </label>
      <label>
        Email:
        <input
          type="email"
          name="email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
        />
      </label>
      <button type="submit">Submit</button>
    </form>
  );
}
```

Here, `name` and `email` are separate states for handling form input. But, when we have to manage many inputs, following this method can be more verbose and repetitive. 

To avoid these problems, we can use a single state `object` with multiple properties to manage the required number of `inputs`. An example of such a use case is given below:

```jsx
import React, { useState } from 'react';

const MyForm = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
  });

  const handleInputChange = (stateName, value) => {
    setFormData({ ...formData, [stateName]: value });
  };

  const handleSubmit = (event) => {
    // Handle form submission
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={(e) => handleInputChange('name', e.target.value)}
        />
      </label>
      <label>
        Email:
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={(e) => handleInputChange('email', e.target.value)}
        />
      </label>
      <button type="submit">Submit</button>
    </form>
  );
};

export default MyForm;
```

In this example, we are using the `useState` hook to create a state object with two properties: `name` and `email`. We are then rendering two input elements, one for each property in the state object.

In the `handleInputChange` event handler, we're passing in two arguments: `stateName` and `value`. The `stateName` argument corresponds to the property name in the state object that we want to update, and the `value` argument is the new value for that property.

## Rendering multiple input elements

There are two ways to render multiple input fields in a form using React:

1. **Rendering Input Fields Statically:**

   This involves manually rendering each input field in the form using JSX. This approach is useful when the number of input fields is small. However, it can become tedious and error-prone when the number of input fields is large. All the previous form examples were manually rendered.

   See the example of rendering input fields statically in a React component:

   ```jsx
   import React, { useState } from 'react';

   const MyForm = () => {
     const [formData, setFormData] = useState({
        name: '',
        email: '',
     });
     
     const handleSubmit = (event) => {
       // Handle form submission
     };
   
     return (
       <form onSubmit={handleSubmit}>
         <label>
           Name:
           <input type="text" value={formData.name}/>
         </label>
         <label>
           Email:
           <input type="email" value={formData.email}/>
         </label>
         <button type="submit">Submit</button>
       </form>
     );
   };
   ```
   Here we have two input fields for `Name` and `Email`, which are manually rendered and have taken 8 lines of code. 

   Now let's see the below example and compare them.

2. **Rendering Input Fields Dynamically Using an Array:**

   This involves creating an array of input field objects and then mapping over that array to dynamically render each input field in the form. This approach is useful when the number of input fields is large and dynamic, or when you need to render the same type of input field multiple times with different configurations.

   Here's an example of rendering input fields dynamically using an array in a React component:

   ```jsx
   import React, { useState } from 'react';

   const MyForm = () => {
    const [formData, setFormData] = useState({
      name: '',
      email: '',
      password: '',
      age: '',
    });

    const inputFields = [
      { stateName: 'name', type: 'text' },
      { stateName: 'email', type: 'email' },
      { stateName: 'password', type: 'password' },
      { stateName: 'age', type: 'number' },
      //........... rest of the input fields information
    ];

    const handleSubmit = (event) => {
      // Handle form submission
    };

    const handleInputChange = (stateName, value) => {
      setFormData({ ...formData, [stateName]: value });
    };

    return (
      <form>
        {inputFields.map((field, index) => (
          <label key={index}>
            {field.stateName}:
            <input
              type={field.type}
              value={formData[field.stateName]}
              onChange={(e) => handleInputChange(field.stateName, e.target.value)}
            />
          </label>
        ))}
        <button onClick={handleSubmit}>Submit</button>
      </form>
    );
   };

   export default MyForm;
   ```

   In this example, the `inputFields` array is an array of objects that represent each input field in the form. Each object has two properties: `stateName` and `type`. The `stateName` property corresponds to the property name in the `formData` object, and the `type` property corresponds to the type attribute of the input element. The component renders the form with each input field using the `map` method to loop through the `inputFields` array. For each input field, it renders a `label` element with the field name and an `input` element with the corresponding `type` and `value` attributes. When the user enters data into the input field, the `onChange` event is fired, which calls the `handleInputChange` function to update the `formData` object. When the user clicks the `Submit` button, the `handleSubmit` function is called. 

   And it's taking just 9–10 lines of code for four input fields.

## Input fields with multiple properties

We can add `properties` to input fields by adding `properties` in the `inputFields` array.

```jsx
const inputFields = [
    {stateName:'name', type:"text",placeHolder:'Enter Name', defaultValue:"test name"},
    {stateName:'email', type:"email",placeHolder:'Enter Email', defaultValue:"test@a.c"},
    //........... rest of the input fields information
]
```

Now, we render the `input` fields

```jsx
<form>
    {inputFields.map((field, index) => (
        <label key={index}>
          {field.stateName}:
          <input type={field.type} 
                value={formData[field.stateName]} 
                onChange={(e) => handleInputChange(field.stateName, e.target.value)}
                defaultValue={field.defaultValue}
                placeholder={field.placeholder}
          />
        </label>

    ))}
    <button onClick={handleSubmit}>Submit</button>
</form>
```

## Input fields with variable number of properties

Sometimes all the `inputs` may not have same number of properties. In such a case, we can use the `spread operator(...)` to pass the propertirs to the input field. Then the `JSX` for the avobe example will be:

```jsx
<form>
    {inputFields.map((field, index) => (
        <label key={index}>
            {field.stateName}:
            <input value={formData[field.stateName]} 
                   onChange={(e) => handleInputChange(field.stateName, e.target.value)}
                   {...field}
            />
        </label>
    ))}
    <button onClick={handleSubmit}>Submit</button>
</form>
```

## Input fields with with different controll types
By including `properties` in the `inputFields` array, we can assign additional attributes to the input fields.

```jsx
const inputFields = [
  {
    stateName: "name",
    name: "Name",
    type: "text",
    placeholder: "Enter your name",
    defaultValue: "",
  },
  {
    stateName: "email",
    name: "Email",
    type: "email",
    placeholder: "Enter your email",
    defaultValue: "",
  },
  {
    stateName: "gender",
    name: "Gender",
    type: "select",
    options: [
      { value: "", label: "--Please choose an option--" },
      { value: "male", label: "Male" },
      { value: "female", label: "Female" },
      { value: "other", label: "Other" },
    ],
  },
  {
    stateName: "age",
    name: "Age",
    type: "number",
    placeholder: "Enter your age",
    defaultValue: "",
  },
];
```
The state will look like: 
```jsx
const [formData, setFormData] = useState({
  name: '',
  email: '',
  gender: '',
  // rest of the form data fields
});
```

Now, we render the `input` fields

```jsx
<form>
  {inputFields.map((field, index) => (
    <label key={index}>
      {field.stateName}:
      {field.type === "select" ? (
        <select
          value={formData[field.stateName]}
          onChange={(e) => handleInputChange(field.stateName, e.target.value)}
          {...field}
        >
          {field.options.map((option, index) => (
            <option key={index} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      ) : (
        <input
          type={field.type}
          value={formData[field.stateName]}
          onChange={(e) => handleInputChange(field.stateName, e.target.value)}
          {...field}
        />
      )}
    </label>
  ))}
  <button onClick={handleSubmit}>Submit</button>
</form>
```

When the `type` property of an input field is set to `select`, the code renders a `select` element with options defined in the `options` property of the input field. The currently selected option is determined by the value of the corresponding key in the `formData` state. Whenever a user selects a new option from the `select` element, the `handleInputChange` function is called with the field's `stateName` and the new selected value as arguments, updating the value in the `formData` state.

On the other hand, if the `type` property of an input field is not set to `select`, the code renders an `input` element with the `type`, `value`, `placeholder`, and `defaultValue` properties defined in the corresponding input field. The value of the input field is determined by the corresponding key in the `formData` state, and the `handleInputChange` function is called whenever a user types into the input element, passing the field's `stateName` and the new input value as arguments, updating the value in the `formData` state.